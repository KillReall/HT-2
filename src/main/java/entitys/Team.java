package entitys;

import lombok.Data;

@Data
public class Team {
    private int id;
    private String name;
}
